
% ----------------------------------------------------------------
\section{Introduction}
\label{sec:intro}

This archive contains the HAWC2 model for the two-bladed, onshore DeepWind 5 MW Vertical-Axis Wind Turbine (VAWT).
The archive is automatically created from source files located on a GitLab repository; see Sec.~\ref{sec:repo} for more details.
An offshore model of the Deepwind VAWT was first developed as part of the DeepWind project, and a report has been published with the original loads analysis of that model \cite{Verelst2014}.
The loads analysis contained a basic analysis of a selection of mean and fatigue loads for a few sea states.
The model was then converted to an onshore model for an MSc. thesis project \cite{Galinos2015} in order to evaluate the accuracy of VAWT design loads when calculated according to IEC~61400-1 \cite{IEC2005}.
A diagram of the DeepWind VAWT is given in Fig.~\ref{fig:vis}.

\begin{figure}
\centering
\includegraphics[width=3in]{deepwind_5mw_vawt_vis.png}
\caption{Visualisation of the DeepWind 5 MW VAWT.}
\label{fig:vis}
\end{figure}

% ----------------------------------------------------------------
\section{Model history and versioning}

There are two sets of model versions: those for the original offshore model and those for the subsequent onshore model.
This archive and repository follow the onshore versioning.
Version 1.0 of the onshore model is the first iteration of the onshore model, which was presumably created from version 2.2.0 of the offshore model, the version referenced in the final report \cite{Verelst2014}.
The model versioning is of the form \texttt{major.minor}, where a minor revision indicates that no model parameters have changed that would affect simulation results.
A description of the model versions can be found in Table~\ref{tab:versions}.

\begin{table}\centering
	\caption{Summary of model versions}
	\begin{tabular}{c|p{4in}}\hline
	\textbf{Version} & \textbf{Notes/Changes to HAWC2 files} \\\hline
	1.0 & Original files distributed on \url{www.hawc2.dk} \\\hline
	1.1 & Changes to file structure and comments.
		No changes to model parameters.
	\end{tabular}
	\label{tab:versions}
\end{table}

% ----------------------------------------------------------------
\section{Model structure}

The VAWT is constructed out of 4 tubular structures---3 rotating, 1 non-rotating---and two blades.
There is a 143-m flexible tower to which the two blades are mounted.
This is mounted upon a 6-m flexible body, which in turn rests upon a 1-m rigid bearing.
The tower, subtower and bearing all rotate with the blades.
Lastly, a 20-m rigid base connects the rotating parts to the ground.
The model is controlled using a basic generator DLL.
The turbulence box in the turbulent simulation is assumed to extend from the top of the rotor to the ground, and is wider than the rotor diameter (130~m vs. 121~m) in case of swaying or potential offshore simulations.
A summary of important parameters is given in Table~\ref{tab:parms}.

\begin{table}\centering
\caption{Parameters for onshore DeepWind 5 MW VAWT}
\begin{tabular}{l|l|l} \hline
\textbf{Property} & \textbf{Unit} & \textbf{Value} \\\hline
Number of blades & [-] & 2 \\
Rotor height & [m] & 143 \\
Rotor diameter & [m] & 121 \\
Bottom/top of rotor & [m] & 27, 170 \\
Rotor rated electrical power & [kW] & 5000 \\
Blade length & [m] & 86.4 \\
Number of generators & [-] & 3 \\
Rated rotor speed & [rad/s] & 0.62 \\
 &[rpm] & 5.95 \\
 & [Hz] & 0.099 \\
Cut-in wind speed & [m/s] & 4 \\
Cut-out wind speed & [m/s] & 25 \\\hline
\end{tabular}
\label{tab:parms}
\end{table}

% ----------------------------------------------------------------
\section{Repository structure}
\label{sec:repo}

The archive is created automatically from source files located in a GitLab repository (``repo'')\footnote{\url{https://gitlab.windenergy.dtu.dk/hawc-reference-models/deepwind-5mw-vawt}}.
The repo uses a branch structure to track the different model versions.
For example, version 1.0 of the model can be found in branch \texttt{v1.0}.
These branches are locked to prevent unauthorized pushes or merges.
Updates to comments and/or line order are allowed within a released version.
However, any changes to model parameters or outputs are not allowed.
If a user requires changes to a released version, they must do so on a separate branch, and these changes cannot be merged.
Updates to the model itself (e.g., fixing bugs or improving the controller) must be submitted as a merge request on the master branch and then reviewed/accepted by the repo responsible.

This repo contains the source code for this report, some utilities for model generation, and the HAWC2 input files for the model.
The folder structure for the input files is as shown:
\begin{verbatim}
- control/  <-- control DLLs go here
   - wpdata.100  <-- text files for DTU Basic Controller
- data/  <-- files for bodies and aeroparameters
- htc/  <-- HAWC2 files
   - deepwind_5mw_vawt.htc  <-- 100-s steady simulation, 8 m/s
\end{verbatim}

Only the 100-s, steady-wind htc file is tracked with the repo; the other htc files in the archive are created automaticallly via GitLab's CI/CD capabilities.
The CI/CD pipelines also create the archive for distribution.
If, for some reason, a user has cloned the repo but wishes to regenerate the extra HAWC2 files locally, they may look into the scripts in the \texttt{utils/} folder.
Note that these scripts have dependencies that must be satisfied in order to run.
Alternatively, the most recent build of the zip file can be downloaded by clicking this link:

\url{https://gitlab.windenergy.dtu.dk/hawc-reference-models/deepwind-5mw-vawt/-/jobs/artifacts/master/raw/deepwind-5mw-vawt.zip?job=make_32bit_zip}.

If available, the zip files corresponding to specific versions may be downloaded by replacing the word ``master'' in the link above with the respective branch name (e.g., ``v1.1'').


% ----------------------------------------------------------------
\section{Bugs, model improvements and questions}

If you as a user notice that there is a bug in the model (e.g., a sign error or a parameter that doesn't match the model documentation), then we kindly ask that you notify us by submitting an issue on GitLab.
Navigate to the repo on GitLab, then click on ``Issues'' in the side bar.
The Issue Tracker on GitLab is how we keep track of issues with the model and things that should be fixed for the next model release.

If you have questions about a model that are not answered by this document, then please email \href{mailto:hawc2@windenergy.dtu.dk}{hawc2@windenergy.dtu.dk}.
