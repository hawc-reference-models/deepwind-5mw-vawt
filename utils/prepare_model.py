# -*- coding: utf-8 -*-
"""Prepare the zip file for the model
"""
from refutils.utils import ReferenceModel


class Deepwind5MWVAWT(ReferenceModel):
    def __init__(self):
        model_kwargs = dict(cut_in=4, cut_out=25, n_wsp=22, constant_power=1, dt=40,
                            tstart=100, wsp=23, tint=0.157, tb_wid=130, tb_ht=170, tower_shadow=4)
        model_kwargs['tint'] = 0.14*(0.75*model_kwargs['wsp'] + 5.6) / model_kwargs['wsp']
        dll_list = [('generator_deepwind.dll', 'generator.dll')]
        ReferenceModel.__init__(self,
                                model_path_name='deepwind-5mw-vawt',
                                htc_basename='deepwind_5mw_vawt',
                                model_kwargs=model_kwargs,
                                dll_list=dll_list,
                                step=True, turb=True, hs2=False)

if __name__ == '__main__':
    model = Deepwind5MWVAWT()
    model.prepare_model()
    #model.test_with_hawc2binary_master()
